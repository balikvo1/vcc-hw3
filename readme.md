# 1 Dockerfile pro webovou aplikaci

- Pouziju jednoduchou nodejs webovou sluzbu z domaciho ukolu z NI-AM2.
```
var http = require("http");
var server = http.createServer(function (req, resp) {
    resp.writeHead(200);
    resp.end("Hello ".concat(req.url.split('/')[1], "\r\n"));
});
server.listen(8080);
```
- Dockerfile
```
FROM node
WORKDIR /home/node
EXPOSE 8080/tcp
COPY src/main.js .
CMD node main.js
```
- ![](Pasted%20image%2020220608000450.png)
- ![](Pasted%20image%2020220608000533.png)
- Pri spousteni kontejneru je pak potreba vystavit port 8080 pomoci `-p <port>:8080`
	- ![](Pasted%20image%2020220608020044.png)
	- 
# 2 Analyza obrazu
- dle doporuceni z cviceni, pouziji nastroj dive
- na virtualce v cloud FIT mi nastroj dive nebezel moc plynule
	- nejdriv tedy `docker save > service.tar`
	- pomoci scp ho prekopiruju k sobe na PC
	- pak `dive service.tar --source docker-archive`
- nastroj nezobrazi vrstvy, kde nic nebylo pridano/ zmeneno/ smazano 
	- to je snadno videt pri porovnani s vystupem `docker history service`
	- ![](Pasted%20image%2020220608011439.png)
	-  nebo kdyz rozbalim `tar -xvf service.tar` a ..
		- idcka v poli Layers jsou prave ty, ktere vidim v dive
		- ![](Pasted%20image%2020220608013312.png)
		- zde jsou vrstvy oznacene `empty_layer: true` temi, ktere se nezobrazi
		- ![](Pasted%20image%2020220608012819.png)
	- Zde tedy memu Dockerfile odpovida pouze posledni vrstva, kde se kopiruje zdrojovy kod sluzby
	- ![](Pasted%20image%2020220608011639.png)
	- nahlednutim do Dockerfile pro node:latest lze snadno zjistit, kde konci vrstvy node image
		- https://github.com/nodejs/docker-node/blob/90065897cdca681a20c3383f28b436bc2434928f/18/bullseye/Dockerfile
		- ![](Pasted%20image%2020220608014906.png)
		- neoznacene vrstvy jsou podklad pro node image, ktere uz jsem nezkoumal
		- zajimave je, ze hranice pro vrstvy node nekoresponduje s hranicemi u dat vytvoreni
			- hadam, ze prikaz pro novou verzi node vytvoril identicke zmeny nad stejnym zakladem (stejny hash) a tak se vrstva mohla prepouzit